from django.contrib import admin
from .models import YouTubeChannels,YouTubeVideos

admin.site.register(YouTubeVideos)
admin.site.register(YouTubeChannels)