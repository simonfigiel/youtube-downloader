import requests
from bs4 import BeautifulSoup
from pytube import YouTube
import logging as log
import requests

from .models import YouTubeChannels,YouTubeVideos
# yt_subscritpions = {"pyta.pl": "UC7xj2xj0EtGt8EZ3ERJf-IQ/videos",
#                     'Stanisław Michalkiewicz':'UCtnpvFVFVlS8sf5blhTr1lQ/videos',
#                     'Marcin Rola':'UCjFvWMiZnEtdPAD008RzYPA/videos'
#
#                     }
# yt_cons_url="https://www.youtube.com/channel/"

#TODO 1)Use CreateView to adding some data do DB 2)finish CollectChannelsToCheck
def CollectChannelsToCheck():
    queryset = YouTubeChannels.object.filter(update=True)
    return True

def PyTube(url,file_format,des_path):
    yt = YouTube(url)

    if file_format =='MP3':
        log.warning(f'MP3 stream: {yt.streams.filter(only_audio=True,adaptive=True).all()[0]}')

        stream = yt.streams.filter(only_audio=True,adaptive=True).all()[0]
    else:

        log.warning(f'Video stream: {yt.streams.filter(adaptive=True).all()[0]}')
        stream = yt.streams.filter(adaptive=True).all()[0]

    stream.download(des_path)
    return True


class YTDownloder:
    def __init__(self, subscritpions):
        self.subscritpions = subscritpions

    def get_links(self, *channels):
        return ((channel, self.subscritpions[channel]) for channel in channels)

    def requests(self, url):
        r = requests.get(url)
        return BeautifulSoup(r.content, "html.parser")

    def processor(self):
        pass

    def get_image_url(self):
        pass

    def get_video_metadata(self):
        soupeddata = requests()
        yt_links = soupeddata.find_all("a", class_="yt-uix-tile-link")
        return ((x.get("href"), x.get("title")) for x in yt_links)


if '__main__' == '__main__':
    print('success')
    # test = YTDownloder(yt_subscritpions)
    # a = test.get_links
    # list(a)
