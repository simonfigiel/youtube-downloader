from django import forms
import os
from .models import YouTubeChannels,YouTubeVideos,Genres


STATUS_CHOICES = (
                 ("Video",("Video")),
                 ("MP3",("MP3")),)

GENRES_TYPES = (
                ("News", "News"),
                ("Politics", "Politics"),
                ("Music", "Music"),
                ("Travel", "Travel"),
                ("Education", "Education"),
                ("Others", "Others"),)

class AddGenresForm(forms.ModelForm):
    genres_type = forms.CharField(max_length=25)

    class Meta:
        model = Genres
        fields = ['genres_type',]

    def cleaned_data(self):
        '''ensure that genres is capital'''
        return self.cleaned_data['genres_type'].capitalize()


class DownloadForm(forms.Form):
    url         = forms.URLField(initial='https://www.youtube.com/watch?v=qB4KPfBx7UQ')
    file_format = forms.ChoiceField(choices=STATUS_CHOICES)
    #TODO list which allow to choose unrestrained destination
    dest_path   = forms.CharField(initial=os.path.expanduser("~/Desktop/"))


class AddYouTubeChannelForm(forms.ModelForm):
    channel_name = forms.CharField(label="Channel name",
                                   widget=forms.TextInput(attrs=
                                                          {"placeholder":"some_name"}))
    url          = forms.URLField(max_length=100)
    genres       = forms.ChoiceField(choices=GENRES_TYPES)

    class Meta:
        model = YouTubeChannels
        fields = [
            'channel_name',
            'url',
            'genres',]
            # 'update'