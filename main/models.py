from django.db import models

class TimeStampeModel(models.Model):
    '''An abstract base class model that provides selfupdating
    ``created`` and ``modified`` fields.'''
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class YouTubeVideos(TimeStampeModel):
    title       = models.CharField(max_length=50)
    url         = models.URLField(max_length=100)
    channel     = models.ForeignKey('YouTubeChannels',models.SET_NULL,blank=True,null=True)
    date        = models.DateField(auto_now_add=True)

class YouTubeChannels(TimeStampeModel):
    channel_name= models.CharField(max_length=50)
    url         = models.URLField(max_length=100)
    genres      = models.CharField(max_length=30,default="Others")
    update      = models.BooleanField(default=True)

    def get_absolute_url(self):
        '''redirect after adding new genres'''
        return u'/add/'

class Genres(models.Model):
    genres_type = models.CharField(max_length=30,unique=True)

    def save(self, *args, **kwargs):
        '''Auto capitalize'''
        for field_name in ['genres_type']:
            val = getattr(self, field_name, False)
            if val:
                setattr(self, field_name, val.capitalize())
        super(Genres, self).save(*args, **kwargs)

    def get_absolute_url(self):
        '''redirect after adding new genres'''
        return u'/add_new_genres/'