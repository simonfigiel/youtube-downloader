from django.contrib import admin
from django.urls import path
from .views import (
            download_single_file,
            download_subscriptions,
            add_new_genres,
            AddNewChannel,
            ListChannels,)

app_name = 'main'

urlpatterns = [
    path('download_single_file/', download_single_file, name="download_single_file"),
    path('add_new_channel/',AddNewChannel.as_view(), name= "add_new_channel"),
    path('add_new_genres/',add_new_genres, name= "add_new_genres"),
    path('list_channels/',ListChannels.as_view(),name= "list_channels"),
    path('', download_subscriptions, name="download_subscriptions"),
]