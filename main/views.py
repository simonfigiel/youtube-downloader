from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.shortcuts import render,reverse,get_object_or_404,get_list_or_404
from django.http import HttpResponse
from .forms import DownloadForm
import logging
from .download_logic import PyTube

from django.views.generic import CreateView,ListView

from .forms import AddYouTubeChannelForm,AddGenresForm
from .models import Genres,YouTubeChannels

logger = logging.getLogger(__name__)

@transaction.non_atomic_requests
# TODO replace generic view by own class/function
class AddNewChannel(SuccessMessageMixin, CreateView):
    template_name='main/add_channel.html'
    form_class = AddYouTubeChannelForm
    success_url = '/add_new_channel/'
    success_message = "Form was created successfully"

    # def get_success_message(self, cleaned_data):
    #     return self.success_message % dict(
    #         cleaned_data,
    #         calculated_field=self.object.calculated_field,
    #     )

@transaction.non_atomic_requests
def add_new_genres(request):
    obj = Genres.objects.all()
    if not obj:
        obj = None
    if request.method == 'POST':
        form = AddGenresForm(request.POST)
        if form.is_valid():
            with transaction.atomic():
                form.save()
                logger.info(f'{form} was saved !')
            obj = Genres.objects.all()
            return render(request, 'main/add_genres.html',{'form': form,'obj':obj})
    else:
        form = AddGenresForm()
    return render(request,
                  'main/add_genres.html',
                  {'form': form,'obj':obj})


class ListChannels(ListView):
    template_name = 'main/channels_list.html'
    queryset = YouTubeChannels.objects.all()


def download_single_file(request):
    if request.method =='POST':
        form = DownloadForm(request.POST)
        if form.is_valid():
            url = form.cleaned_data.get('url')
            file_format = form.cleaned_data.get('file_format')
            dest_path = form.cleaned_data.get('dest_path')

            PyTube(url,file_format,dest_path)
            context = {}
            return render(request, 'main/forms.html', context)

    else:
        form = DownloadForm()

    return render(request,
                  'main/forms.html',
                  {'form':form})

def download_subscriptions(request):
    if request.method =='POST':
        form = DownloadForm(request.POST)
        if form.is_valid():
            url = form.cleaned_data.get('url')
            file_format = form.cleaned_data.get('file_format')
            dest_path = form.cleaned_data.get('dest_path')

            PyTube(url,file_format,dest_path)

            return HttpResponse('THANKS')

    else:
        form = DownloadForm()

    return render(request,
                  'main/subscriptions.html',
                  {'form':""})
